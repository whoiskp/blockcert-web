import { Component, OnInit } from '@angular/core';
import { UnicoinService } from '../../../shared/unicoin.service';

@Component({
  selector: 'app-chain',
  templateUrl: './chain.component.html',
  styleUrls: ['./chain.component.scss']
})
export class ChainComponent implements OnInit {
  unicoin = {};

  constructor(private _unicoinService: UnicoinService) { }

  ngOnInit() {
    this._unicoinService.getChain().subscribe((res: any) => {
      console.log(res);
      this.unicoin = res;
    }, err => {
      console.log(err);
    });
  }
}
