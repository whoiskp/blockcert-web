import { Component, OnInit } from '@angular/core';
import { UnicoinService } from '../../../shared/unicoin.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tx',
  templateUrl: './tx.component.html',
  styleUrls: ['./tx.component.scss']
})
export class TxComponent implements OnInit {
  tx;
  err;
  prk_key;
  result;
  info;

  tx_model = {
    inputAddress: '',
    outputAddress: '',
    amount: '',
    meta_data: ''
  }
  constructor(private _unicoinService: UnicoinService,  private route: ActivatedRoute) { }

  ngOnInit() {
    const txid = +this.route.snapshot.paramMap.get('id');
    console.log(txid);
    if(txid) {
      this.getTx(txid);
    }
  }

  getTx(txid){
    this._unicoinService.getTransInfo(txid).subscribe((res: any) => {
      console.log(res);
      this.tx = res;
      this.err = '';
    }, err => {
      this.err = "not found!"
    });
  }

  onSubmit(){
    this._unicoinService.addNewTransaction(this.tx_model, this.prk_key).subscribe((res: any) => {
      console.log(res);
      this.result = res;
      this.err = '';
    }, err => {
      this.err = "songthing went wrong!"
    });
  }

  mine(){
    this._unicoinService.getMine().subscribe((res: any) => {
      this.info = res;
      console.log(res);
    }, err => {
      console.log(err);
    });
  }
}
