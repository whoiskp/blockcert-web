import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { UnicoinComponent } from './unicoin.component';
import { routing } from './unicoin.routing';
import { ChainComponent } from './chain/chain.component';
import { TxComponent } from './tx/tx.component';
import { AddressComponent } from './address/address.component';
import { NodeComponent } from './node/node.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    routing
  ],
  declarations: [UnicoinComponent, ChainComponent, TxComponent, AddressComponent, NodeComponent]
})
export class UnicoinModule { }
