import { Routes, RouterModule } from '@angular/router'

import { UnicoinComponent } from './unicoin.component'
import { AddressComponent } from './address/address.component'
import { ChainComponent } from './chain/chain.component'
import { NodeComponent } from './node/node.component'
import { TxComponent } from './tx/tx.component'

const routes: Routes = [
    {
        path: '',
        component: UnicoinComponent,
        children: [
            { path: 'address', component: AddressComponent },
            { path: 'chain', component: ChainComponent },
            { path: 'nodes', component: NodeComponent },
            { path: 'tx', component: TxComponent },
            // { path: 'tx/:id', component: TxComponent },
        ]
    }
]

export const routing = RouterModule.forChild(routes)