import { Component, OnInit } from '@angular/core';
import { UnicoinService } from '../../../shared/unicoin.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  data;
  constructor(private _unicoinService: UnicoinService) { }

  ngOnInit() {
  }
  
  getAddress() {
    this._unicoinService.getWalletArres().subscribe((res: any) => {
      console.log(res);
      this.data = res;
    }, err => {
      console.log(err);
    });
  }
}
