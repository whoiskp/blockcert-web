import { Component, OnInit } from '@angular/core';
import { UnicoinService } from '../../shared/unicoin.service';


@Component({
  selector: 'app-unicoin',
  templateUrl: './unicoin.component.html',
  styleUrls: ['./unicoin.component.scss']
})
export class UnicoinComponent implements OnInit {
  defaultLink = "#/pages/unicoin/";
  routes = [];
 
  constructor(private _unicoinService: UnicoinService) {
    this.routes = [
      { "name": "address", "link": this.defaultLink + "address" },
      { "name": "chain", "link": this.defaultLink + "chain" },
      { "name": "nodes", "link": this.defaultLink + "nodes" },
      { "name": "tx", "link": this.defaultLink + "tx" }
    ]
  }

  ngOnInit() {
  }
}
