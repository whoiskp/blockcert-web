import { Component, OnInit } from '@angular/core';
import { UnicoinService } from '../../../shared/unicoin.service';

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnInit {
  nodes = [];
  infoRes;
  constructor(private _unicoinService: UnicoinService) { }

  ngOnInit() {
    this._unicoinService.getNodes().subscribe((res: any) => {
      console.log(res);
      this.nodes = res;
    }, err => {
      console.log(err);
    });
  }

  registerNodes(nodeip) {
    console.log(nodeip);
    console.log(this.nodes.indexOf(nodeip))
    if (this.nodes.indexOf(nodeip) < 0) {
      this._unicoinService.registerNode({ 'nodes': [nodeip] }).subscribe((res: any) => {
        this.nodes = res['total_nodes'];
      }, err => {
        console.log(err);
      });
    }
  }

  resolveNodes() {
    this._unicoinService.resolve_node().subscribe((res: any) => {
      this.infoRes = res;
    }, err => {
      this.infoRes = null;
      console.log(err);
    });
  }
}
