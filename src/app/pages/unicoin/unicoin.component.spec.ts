import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnicoinComponent } from './unicoin.component';

describe('UnicoinComponent', () => {
  let component: UnicoinComponent;
  let fixture: ComponentFixture<UnicoinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnicoinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnicoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
