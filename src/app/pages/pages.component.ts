import { Component, OnInit } from '@angular/core';
import { UnicoinService } from '../shared/unicoin.service';
import { API_URL } from '../shared/config';
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  nodeIp;
  urlApi;
  constructor(private _unicoinService: UnicoinService) { }

  ngOnInit() {
    this._unicoinService.getNodeID().subscribe((res: any) => {
      this.nodeIp = res._body;
      this.urlApi = this._unicoinService.getAPI_URL();
    }, err => {
      console.log(err);
    });
  }

  connect(nodeip) {
    console.log(nodeip);
    this._unicoinService.setAPI_URL(nodeip);
    this._unicoinService.getNodeID().subscribe((res: any) => {
      if (res._body) {
        this.nodeIp = res._body;
        this.urlApi = this._unicoinService.getAPI_URL();
      } else {
        this.urlApi = API_URL;
        this._unicoinService.setAPI_URL(API_URL);
      }
    }, err => {
      console.log(err);
    });
  }
}
