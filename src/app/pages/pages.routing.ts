import { Routes, RouterModule } from '@angular/router'
import { ModuleWithProviders } from '@angular/core'

import { PagesComponent } from './pages.component'
const routes: Routes = [
    {
        path: 'pages',
        component: PagesComponent,
        children: [
            {
                path: 'unicert',
                loadChildren: './unicert/unicert.module#UnicertModule'
            },
            {
                path: 'unicoin',
                loadChildren: './unicoin/unicoin.module#UnicoinModule'
            },
        ]
    }
]

export const routing: ModuleWithProviders = RouterModule.forChild(routes)