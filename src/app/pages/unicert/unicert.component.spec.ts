import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnicertComponent } from './unicert.component';

describe('UnicertComponent', () => {
  let component: UnicertComponent;
  let fixture: ComponentFixture<UnicertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnicertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnicertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
