export const settings = {
    actions: {
        columnTitle: '',
        add: false,
        edit: true,
        delete: false
    },
    add: {
        addButtonContent: '<i class="fa fa-plus"></i>',
        createButtonContent: '<i class="fa fa-check"></i>',
        cancelButtonContent: '<i class="fa fa-times"></i>',
    },
    edit: {
        editButtonContent: '<i class="fa fa-pencil"></i>',
        saveButtonContent: '<i class="fa fa-check"></i>',
        cancelButtonContent: '<i class="fa fa-times"></i>',
    },
    delete: {
        deleteButtonContent: '<i class="fa fa-trash"></i>',
        confirmDelete: true,
    },
    pager: {
        perPage: 6,
    },
    columns: {
        id: {
            title: '#',
            type: 'number',
        },
        firstName: {
            title: 'Họ và Tên',
            type: 'string',
        },
        lastName: {
            title: 'Chức danh',
            type: 'string',
        },
        username: {
            title: 'Công ty',
            type: 'string',
        }
    },
};