import { Component, OnInit } from '@angular/core';
import { UnicoinService } from '../../../shared/unicoin.service';
import { LocalDataSource } from 'ng2-smart-table';
import { settings } from './config';
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import * as _ from 'underscore';
type AOA = any[][];

@Component({
  selector: 'app-issuer',
  templateUrl: './issuer.component.html',
  styleUrls: ['./issuer.component.scss']
})
export class IssuerComponent implements OnInit {
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  rand;
  titles;

  issuerInfo = {
    "id": "d9ea7e6523fe4b9a91d3ba81f35b872c",
    "logo": "./assets/images/logo-ptit.png",
    "name": "PTIT HCM",
    "url": "http://portal.ptit.edu.vn/",
    "email": "ptithcm@edu.vn"
  };

  certBatchName;
  signPrikey;

  issue_data = [];

  issuerSign = {
    "jobTitle": "Director",
    "image": "./assets/images/signature.png",
    "name": "Mr Wonderful"
  }

  // array of all items to be paged
  private allItems: AOA;

  // pager object
  pager: any = {};

  // paged items
  pagedItems: AOA;
  constructor(private _unicoinService: UnicoinService,) { }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    // wire up file reader
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read wordbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.allItems = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      this.titles = this.allItems.shift();
      this.setPage(1);
    }
    reader.readAsBinaryString(target.files[0]);
  }

  onIssue() {
    let base64logo = this.getBase64Image(document.getElementById("logo-issuer"));
    let base64Sign = this.getBase64Image(document.getElementById("sign-issuer"));
    console.log(base64Sign);
    this.titles.push("Download Cert")
    this.allItems.forEach(item => {
      let cert_info = {
        "issuedOn": new Date().toLocaleString(),
        "recipient": {
          "id": item[0],
          "name": item[1],
          "email": item[2],
        },
        "badge": {
          "issuer": {
            "url": this.issuerInfo.url,
            "name": this.issuerInfo.name,
            "email": this.issuerInfo.email,
            "type": "Profile",
            "id": this.issuerInfo.id,
            "image": base64logo
          },
          "name": this.certBatchName,
          "criteria": {
            "narrative": item[3]
          },
          "description": item[4],
          "signatureLines": {
            "jobTitle": this.issuerSign.jobTitle,
            "name": this.issuerSign.name,
            "image": base64Sign
          }
        },
      }
      this.issue_data.push(cert_info);
      item.push("download");
    });
    
    this._unicoinService.gen_cert(this.issue_data, this.issuerInfo.id, this.signPrikey).subscribe((res: any) => {
      let  uni_signers = res['data'];
      for (let index = 0; index < this.issue_data.length; index++) {
        this.issue_data[index]['unicertSignature'] = uni_signers[index];
      }
      console.log(res);
    }, err => {
      console.log("songthing went wrong!");
    });    

    this.issue_data.forEach(el => {
      console.log(el);
    });
  }

  export(): void {
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.allItems);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }

  random(): void {
    let position = Math.floor(Math.random() * this.allItems.length);
    this.rand = this.allItems[position];
    this.allItems.splice(position, 1);
    swal({
      type: 'success',
      title: 'This Certificate is valid!'
    });

    let totalPages = Math.ceil(this.allItems.length / 5);
    let current = this.pager.currentPage > totalPages ? totalPages : this.pager.currentPage;
    this.setPage(current);
    console.log(this.allItems.length);
    console.log(this.rand[1] + " " + this.rand[2] + " " + this.rand[3]);
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 5) {
    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    let startPage: number, endPage: number;

    if (totalPages <= 5) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 3) {
        startPage = 1;
        endPage = 5;
      } else if (currentPage + 1 >= totalPages) {
        startPage = totalPages - 4;
        endPage = totalPages;
      } else {
        startPage = currentPage - 2;
        endPage = currentPage + 2;
      }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = _.range(startPage, endPage + 1);

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }

  saveTextAsFile(data, filename) {

    if (!data) {
      console.error('Console.save: No data')
      return;
    }

    if (!filename) filename = 'console.json'

    var blob = new Blob([data], { type: 'text/plain' }),
      e = document.createEvent('MouseEvents'),
      a = document.createElement('a')
    // FOR IE:

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, filename);
    }
    else {
      var e = document.createEvent('MouseEvents'),
        a = document.createElement('a');

      a.download = filename;
      a.href = window.URL.createObjectURL(blob);
      a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
      e.initEvent('click', true, false);
      a.dispatchEvent(e);
    }
  }


  expFile(index_cert) {
    console.log(index_cert);
    var fileText = JSON.stringify(this.issue_data[index_cert]);
    var fileName = this.allItems[index_cert][0] + ".json";
    this.saveTextAsFile(fileText, fileName);
  }

  private base64textString: String = "";

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    console.log(btoa(binaryString));
  }

  getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    // return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    return dataURL;
  }
}
