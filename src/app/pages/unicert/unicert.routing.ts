import { Routes, RouterModule } from '@angular/router'

import { UnicertComponent } from './unicert.component'
import { IssuerComponent } from './issuer/issuer.component'
import { VerifyComponent } from './verify/verify.component'
import { RecipientComponent } from './recipient/recipient.component'
import { CertInfoComponent } from './cert-info/cert-info.component'

const routes: Routes = [
    {
        path: '',
        component: UnicertComponent,
        children: [
            { path: 'issuer', component: IssuerComponent },
            { path: 'verify', component: VerifyComponent },
            { path: 'recipient', component: RecipientComponent },
            { path: 'cert-info', component: CertInfoComponent },
        ]
    }
]

export const routing = RouterModule.forChild(routes)