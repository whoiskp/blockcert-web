import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unicert',
  templateUrl: './unicert.component.html',
  styleUrls: ['./unicert.component.scss']
})
export class UnicertComponent implements OnInit {
  defaultLink = "#/pages/unicert/";
  routes = [];
  constructor() {
    this.routes = [
      { "name": "Issuer", "link": this.defaultLink + "issuer" },
      { "name": "Cert Info", "link": this.defaultLink + "cert-info" }
    ]
  }

  ngOnInit() {
  }

}
