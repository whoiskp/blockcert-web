import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routing } from './unicert.routing'
import { IssuerComponent } from './issuer/issuer.component';
import { VerifyComponent } from './verify/verify.component';
import { UnicertComponent } from './unicert.component';
import { RecipientComponent } from './recipient/recipient.component';
import { CertInfoComponent } from './cert-info/cert-info.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    routing
  ],
  declarations: [IssuerComponent, VerifyComponent, UnicertComponent, RecipientComponent, CertInfoComponent]
})
export class UnicertModule { }
