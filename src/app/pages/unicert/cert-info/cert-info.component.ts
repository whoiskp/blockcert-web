import { Component, OnInit } from '@angular/core';
import { UnicoinService } from '../../../shared/unicoin.service';

@Component({
  selector: 'app-cert-info',
  templateUrl: './cert-info.component.html',
  styleUrls: ['./cert-info.component.scss']
})
export class CertInfoComponent implements OnInit {

  certInfo;
  validInfo;
  constructor(private _unicoinService: UnicoinService) { }


  ngOnInit() {
  }

  onFileChange(evt) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      let content = fileReader.result.toString();
      this.certInfo = JSON.parse(content);
      console.log(this.certInfo);
    }
    fileReader.readAsText(evt.target.files[0]);
  }

  onCheckValid() {
    this._unicoinService.valid_cert(this.certInfo).subscribe((res: any) => {
      this.validInfo = res['data'];
      console.log(this.validInfo);
    }, err => {
      console.log("songthing went wrong!");
    });
  }
}
