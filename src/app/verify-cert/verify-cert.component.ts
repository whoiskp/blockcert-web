import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import * as _ from 'underscore';
type AOA = any[][];
declare var $: any;
enum State {
  ready,
  spin,
  complete
}

@Component({
  selector: 'app-verify-cert',
  templateUrl: './verify-cert.component.html',
  styleUrls: ['./verify-cert.component.scss']
})
export class VerifyCertComponent implements OnInit {

  state;
  constructor() { }

  prizes = [{ name: "Giải ba", image: "./assets/svg/giai3.svg", result: [] }, { name: "Giải nhì", image: "./assets/svg/giai2.svg", result: [] }, { name: "Giải nhất", image: "./assets/svg/giai1.svg", result: [] }];

  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  rand;
  position = 0; // from random item
  colorRand = "000";
  currentPrize = 0;
  prizedItems;

  // array of all items to be paged
  private allItems: AOA;
  // pager object
  pager: any = {};

  intervalID;


  ngOnInit() {
    this.state = State.ready;
    this.setPrize(0);
  }
  onFileChange(evt: any) {
    console.log(evt);
    // wire up file reader
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read wordbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.allItems = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      console.log(this.allItems.shift());
    }
    reader.readAsBinaryString(target.files[0]);
  }

  export(): void {
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.allItems);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }

  random(): void {
    this.intervalID = setInterval(() => {
      this.state = State.spin;
      this.position = Math.floor(Math.random() * this.allItems.length);
      this.colorRand = Math.floor(Math.random() * 16777215).toString(16); // color random
      // console.log(this.colorRand);
    }, 100);
  }

  clear() {
    if (this.intervalID) {
      clearInterval(this.intervalID);
      this.xacnhan();
    }
  }

  xacnhan() {
    for (let i = 0; i < this.allItems.length; i++) {
      if (this.allItems[i][0] == this.position) {
        this.rand = this.allItems[i];
        this.allItems.splice(i, 1);
        console.log("index to Remove: " + i);
      }
    }

    console.log(this.rand);
    // swal({
    //   type: 'success',
    //   title: 'Chúc mừng!',
    //   html: `
    //   <h1 style="color: blue">${this.rand[0]}</h1>
    //   <h1 style="color: red">${this.rand[1]}</h1>
    //   <h3>${this.rand[2]}</h3>
    //   <i>${this.rand[3]}</i>
    //   `});
    let str = `<h1 style="color: blue">${this.rand[0]}</h1>`;
    if (this.rand[1]) {
      str += `<h1 style="color: red">${this.rand[1]}</h1>`;
    }
    if (this.rand[2]) {
      str += `<h3>${this.rand[2]}</h3>`;
    }
    if (this.rand[3]) {
      str += `<i>${this.rand[3]}</i>`;
    }
    swal({
      type: 'success',
      title: 'Chúc mừng!',
      html: `${str}`,
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'XÁC NHẬN'
    }).then((result) => {
      if (result.value) {
        // Luu vao ket qua nguoi nhan
        this.position = 0;
        this.state = State.ready;
      };
    });
    // swal({
    //   type: 'success',
    //   title: 'Chúc mừng!',
    //   html: `
    //   <h1 style="color: blue">${this.rand[0]}</h1>
    //   <h1 style="color: red">${this.rand[1]}</h1>
    //   <h3>${this.rand[2]}</h3>
    //   <i>${this.rand[3]}</i>
    //   `,
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Xác nhận',
    //   cancelButtonText: 'Quay lại',
    // }).then((result) => {
    //   if (result.value) {
    //     // Luu vao ket qua nguoi nhan
    //     this.position = 0;
    //     this.state = State.ready;
    //   } else {
    //     this.random();
    //   };
    // });
    // Phân trang
    // let totalPages = Math.ceil(this.allItems.length / 5);
    // let current = this.pager.currentPage > totalPages ? totalPages : this.pager.currentPage;
    // this.setPage(current);
    // console.log(this.allItems.length);
    // console.log(this.rand[1] + " " + this.rand[2] + " " + this.rand[3]);
    $('.swal2-container').fireworks();
    setTimeout(() => {
      $('.swal2-container').fireworks("destroy");
    }, 10000);
  }
  setPrize(prizeIndex: number) {
    if (prizeIndex < 0 || prizeIndex > this.prizes.length) {
      return;
    }

    this.currentPrize = prizeIndex;
    this.prizedItems = this.prizes[this.currentPrize];
  }

}
