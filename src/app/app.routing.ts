import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { NotFoundComponent } from './errors/not-found/not-found.component'
import { VerifyCertComponent } from './verify-cert/verify-cert.component'

export const routes: Routes = [
    // { path: 'unicert', component: VerifyCertComponent },
    { path: '', redirectTo: 'pages', pathMatch: 'full' },
    { path: '**', component: NotFoundComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true, enableTracing: false });