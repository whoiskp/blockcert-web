import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import * as crypto from 'crypto-js';

import { Observable } from 'rxjs/Observable';
import { API_URL } from './config';
import 'rxjs/add/operator/map';

@Injectable()
export class UnicoinService {
  lapi_url = API_URL;

  constructor(private _http: Http) { }

  getNodeID() {
    const url = `${this.lapi_url}/`;
    return this._http.get(url).map((response: Response) => response);
  }

  getChain() {
    const url = `${this.lapi_url}/chain`;
    return this._http.get(url).map((response: Response) => response.json());
  }

  getMine() {
    const url = `${this.lapi_url}/mine`;
    return this._http.get(url).map((response: Response) => response.json());
  }

  getWalletArres() {
    const url = `${this.lapi_url}/address_wallet`;
    return this._http.get(url).map((response: Response) => response.json());
  }

  resolve_node() {
    const url = `${this.lapi_url}/nodes/resolve`;
    return this._http.get(url).map((response: Response) => response.json());
  }

  getTransInfo(txid: string) {
    const url = `${this.lapi_url}/tx/${txid}`;
    return this._http.get(url).map((response: Response) => response.json());
  }

  getNodes() {
    const url = `${this.lapi_url}/nodes`;
    return this._http.get(url).map((response: Response) => response.json());
  }

  addNewTransaction(data: any, prk_key) {
    const url = `${this.lapi_url}/tx`;
    var headers = new Headers();
    this.createAuthorizationHeader(headers, url, data, prk_key);
    headers.append('Content-Type', 'application/json');

    console.log(headers);
    return this._http.post(url, data, {
      headers: headers
    }).map((response: Response) => response.json());
  }

  registerNode(data: any) {
    const url = `${this.lapi_url}/nodes/register`;
    return this._http.post(url, data).map((response: Response) => response.json());
  }

  createAuthorizationHeader(headers: Headers, url, data, prk_key) {
    let body_base = JSON.stringify(data).replace(/\"/g, "'");
    let base_string = `${url}|${body_base}`;
    console.log(base_string);
    console.log(prk_key);
    var hash = crypto.HmacSHA256(base_string, prk_key);
    console.log(hash);
    // var hashInBase64 = crypto.enc.Base64.stringify(hash);
    console.log('hmac: ' + hash);

    headers.append('Authorization', hash);
  }

  gen_cert(data: any, pub_key, prk_key) {
    const url = `${this.lapi_url}/gen_sign_cert`;
    let post_data = {
      'data': data,
      'pub_key': pub_key,
      'prk_key': prk_key,
    }
    return this._http.post(url, post_data).map((response: Response) => response.json());
  }

  valid_cert(cert) {
    const url = `${this.lapi_url}/valid_cert`;
    let post_data = {
      'cert': cert,
    }
    return this._http.post(url, post_data).map((response: Response) => response.json());
  }

  setAPI_URL(host) {
    this.lapi_url = host;
  }

  getAPI_URL() {
    return this.lapi_url;
  }
}
