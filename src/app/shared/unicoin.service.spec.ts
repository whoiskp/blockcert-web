import { TestBed, inject } from '@angular/core/testing';

import { UnicoinService } from './unicoin.service';

describe('UnicoinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnicoinService]
    });
  });

  it('should be created', inject([UnicoinService], (service: UnicoinService) => {
    expect(service).toBeTruthy();
  }));
});
