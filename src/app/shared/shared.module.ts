import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnicoinService } from './unicoin.service'

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    UnicoinService
  ]
})
export class SharedModule { }
