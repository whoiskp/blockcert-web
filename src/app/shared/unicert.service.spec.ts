import { TestBed, inject } from '@angular/core/testing';

import { UnicertService } from './unicert.service';

describe('UnicertService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnicertService]
    });
  });

  it('should be created', inject([UnicertService], (service: UnicertService) => {
    expect(service).toBeTruthy();
  }));
});
